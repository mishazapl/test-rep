<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('system_id')->unique();
            $table->string('title');
            $table->string('image');
            $table->text('description');
            $table->timestamp('first_invoice')->nullable();
            $table->timestamp('last_supplied')->nullable();
            $table->string('url');
            $table->float('price');
            $table->float('amount');
            $table->unsignedInteger('category_google')->nullable();
            $table->timestamps();

            $table->index(['title', 'description']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
