<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/5/18
 * Time: 9:55 PM
 */

namespace App\Service\Products;

use Illuminate\Database\Eloquent\Builder;

class Filters
{
    private $builder;

    /**
     * Filters constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }


    /**
     * @return Builder
     */
    public function getBuilder(): Builder
    {
        return $this->builder;
    }

    public function population(): Filters
    {
        $this->builder = $this->builder
            ->leftJoin('offers_products', 'offers_products.product_id', '=', 'products.system_id')
            ->leftJoin('offers', 'offers_products.offer_id', '=', 'offers.system_id')
            ->whereNotNull('offers.sales')
            ->orderBy('offers.sales', 'DESC');

        return $this;
    }

    /**
     * @return Filters
     */
    public function category(): Filters
    {
        if (request()->has('category'))
           $this->builder = $this->builder->whereHas('categories', function ($query) {
                $query->where('alias', request()->get('category'));
            });

        return $this;
    }

    /**
     * @return Filters
     */
    public function search(): Filters
    {
        $search =  request()->get('search');

        if (request()->has('search')) {
            $this->builder = $this->builder
                ->where('title', 'like', '%' .
                $search . '%')->orWhere('description', 'like', '%' .
                    $search . '%');
        }

        return $this;
    }
}