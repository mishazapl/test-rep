<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/5/18
 * Time: 3:43 PM
 */

namespace App\Service\Products;


use GuzzleHttp\Client;

class ParseProducts
{
    private $guzzle;

    public function __construct()
    {
        $this->guzzle = new Client();
    }

    public function run()
    {
        $result = $this->getProducts();

        return $result->products;
    }

    private function getProducts()
    {
        $query = $this->guzzle->get('https://markethot.ru/export/bestsp');

        return json_decode($query->getBody());
    }
}