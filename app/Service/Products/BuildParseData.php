<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/5/18
 * Time: 4:22 PM
 */

namespace App\Service\Products;

class BuildParseData
{
    private $data;

    private $dataSave = [
        'products' => [],
        'offers' => [],
        'categories' => [],
        'attachCategories' => [],
        'attachOffers' => [],

    ];

    /**
     * SaveParseData constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build(): array
    {
        $this->setDataSave();
        return $this->dataSave;
    }

    private function setDataSave(): void
    {
        foreach ($this->data as $data) {

            $data = (array)$data;
            $this->setProducts($data);
            $this->setOffersAndCategories($data, 'categories', ['categories']);
            $this->setOffersAndCategories($data, 'offers', ['offers']);

        }


    }

    /**
     * @param array $product
     */
    private function setProducts(array $product)
    {
        $product['system_id'] = $product['id'];
        array_push($this->dataSave['products'], array_except($product, ['offers', 'categories', 'images', 'id']));
    }

    /**
     * @param array $data
     * @param string $key
     * @param array $exceptKeys
     */
    public function setOffersAndCategories(array $data, string $key, array $exceptKeys)
    {
        $productId = $data['id'];
        $dataSave = array_only($data, $exceptKeys);

        foreach ($dataSave[$key] as $dataSaving) {

            $dataSaving = (array)$dataSaving;
            array_push($this->dataSave[$key], array_merge($dataSaving, ['system_id' => $dataSaving['id']]));
            $this->setAttachData($dataSaving['id'], $productId, $key);

        }
    }


    /**
     * @param $attachId
     * @param int $id
     * @param string $key
     */
    private function setAttachData($attachId, int $id, string $key)
    {
        array_push($this->dataSave['attach' . ucfirst($key)], [
            'product_id' => $id,
            str_singular($key) . '_id' => $attachId
        ]);
    }


}