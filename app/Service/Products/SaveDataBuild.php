<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 10/5/18
 * Time: 7:09 PM
 */

namespace App\Service\Products;

use Illuminate\Support\Facades\DB;

class SaveDataBuild
{
    private $data;

    /**
     * @param array $data
     */
    public function store(array $data): void
    {
        $this->data = $data;
        $this->saveCallMethods();
    }

    private function saveCallMethods(): void
    {
        $this->save('products', 'products');
        $this->save('categories', 'categories');
        $this->save('offers', 'offers');
        $this->saveAttach('attachOffers', 'offers_products', 'offer_id');
        $this->saveAttach('attachCategories', 'products_categories', 'category_id');
    }

    private function save(string $key, string $table): void
    {
        foreach ($this->data[$key] as $saveData) {

            DB::table($table)
                ->updateOrInsert(
                    [
                        'system_id' => $saveData['system_id']
                    ],
                    $saveData);

        }
    }

    private function saveAttach(string $key, string $table, string $tableKey): void
    {
        foreach ($this->data[$key] as $saveData) {

            DB::table($table)
                ->updateOrInsert(
                    [
                        'product_id' => $saveData['product_id'],
                        $tableKey => $saveData[$tableKey]
                    ],
                    $saveData);

        }
    }
}