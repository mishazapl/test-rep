<?php

namespace App\Http\Controllers;

use App\Category;
use App\Jobs\DailyUpdateProducts;
use App\Product;
use App\Service\Products\Filters;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', [
            'products' => (new Filters(Product::query()))
                ->population()
                ->category()
                ->search()
                ->getBuilder()
                ->paginate(20),
            'categories' => Category::all()
        ]);
    }
}
