<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = [
        'system_id',
        'title',
        'alias',
        'parent',
        'acrm_id',
    ];

    public $hidden = [
        'id'
    ];
}
