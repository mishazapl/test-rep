<?php

namespace App\Jobs;

use App\Service\Products\BuildParseData;
use App\Service\Products\ParseProducts;
use App\Service\Products\SaveDataBuild;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DailyUpdateProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 300;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = (new ParseProducts())->run();
        (new SaveDataBuild)->store((new BuildParseData($data))->build());
    }
}
