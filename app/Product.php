<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $fillable = [
        'system_id',
        'title',
        'image',
        'description',
        'first_invoice',
        'last_supplied',
        'url',
        'price',
        'amount',
        'category_google'
    ];

    public $hidden = [
        'id'
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'products_categories', 'product_id', 'category_id', 'system_id', 'system_id');
    }

    public function offers()
    {
        return $this->belongsToMany('App\Offer', 'offers_products', 'product_id', 'offer_id', 'system_id', 'system_id');
    }
}
