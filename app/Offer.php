<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public $fillable = [
        'system_id',
        'price',
        'amount',
        'sales',
        'article'
    ];

    public $hidden = [
        'id'
    ];
}
