@extends('layouts.app')

@section('styles')
    @parent
    <style>


        .quantity {
            float: left;
            margin-right: 15px;
            background-color: #eee;
            position: relative;
            width: 80px;
            overflow: hidden
        }

        .quantity input {
            margin: 0;
            text-align: center;
            width: 15px;
            height: 15px;
            padding: 0;
            float: right;
            color: #000;
            font-size: 20px;
            border: 0;
            outline: 0;
            background-color: #F6F6F6
        }

        .quantity input.qty {
            position: relative;
            border: 0;
            width: 100%;
            height: 40px;
            padding: 10px 25px 10px 10px;
            text-align: center;
            font-weight: 400;
            font-size: 15px;
            border-radius: 0;
            background-clip: padding-box
        }

        .quantity .minus, .quantity .plus {
            line-height: 0;
            background-clip: padding-box;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            -webkit-background-size: 6px 30px;
            -moz-background-size: 6px 30px;
            color: #bbb;
            font-size: 20px;
            position: absolute;
            height: 50%;
            border: 0;
            right: 0;
            padding: 0;
            width: 25px;
            z-index: 3
        }

        .quantity .minus:hover, .quantity .plus:hover {
            background-color: #dad8da
        }

        .quantity .minus {
            bottom: 0
        }
        .shopping-cart {
            margin-top: 20px;
        }

        .ml-2 {
            margin-left: 2em!important;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="card-header">Категории</div>


                        @foreach($categories as $category)
                        <a href="{{ route('home', ['category' => $category->alias]) }}">{{ $category->title }}</a>
                        <br>
                        @endforeach

                </div>

                <div class="card-body">

                    <div class="card-body">
                        <div class="card-header">Поиск</div>

                        <form action="{{ route('home') }}" method="GET">

                            <input type="text" name="search">
                            <input type="submit" class="btn btn-success" value="Искать">

                        </form>

                    </div>
                    
                    <div class="card-body">
                    {{ $products->render() }}
                        @foreach($products as $product)
                        <!-- PRODUCT -->
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-2 text-center">
                                <img class="img-responsive" src="{{ $product->image }}" alt="prewiew" width="120" height="80">
                            </div>
                            <div class="ml-2 col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                                <h4 class="product-name"><strong>{{ $product->title }}</strong></h4>
                                <h4>
                                    <small>{{ $product->description }}</small>
                                </h4>
                            </div>
                            <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                                <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                    <h6><strong>{{ $product->sales }}</strong></h6>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
